import React, { useState, useEffect } from "react";
import "../loginpage.css";
import { Login } from "./Login";
import { Register } from "./Register";
import { gapi } from "gapi-script";

const clientId =
  "920329733526-l1bnb83vf7qqurpibo0qef2rk3opd3rb.apps.googleusercontent.com";

function LoginPage() {
  const [currentForm, setCurrentForm] = useState("login");

  const toggleForm = (formName) => {
    setCurrentForm(formName);
  };

  useEffect(() => {
    function start() {
      gapi.client.init({
        clientId: clientId,
        scope: "",
      });
    }
    gapi.load("client:auth2", start);
  }, []);

  return (
    <div className="LoginPage">
      <div className="main">
        {currentForm === "login" ? (
          <Login onFormSwitch={toggleForm} />
        ) : (
          <Register onFormSwitch={toggleForm} />
        )}
      </div>
    </div>
  );
}

export default LoginPage;
