// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyBZNDIhHYEot-Pqfsjp-EHYgrWTD3B6FxU",
  authDomain: "miflix-3d47c.firebaseapp.com",
  projectId: "miflix-3d47c",
  storageBucket: "miflix-3d47c.appspot.com",
  messagingSenderId: "650845679068",
  appId: "1:650845679068:web:3522239127cc5fed5af319",
  measurementId: "G-JT9ZCRWFPL",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
