import React from "react";
import { GoogleLogout } from "react-google-login";
import { useNavigate } from "react-router-dom";

const clientId =
  "920329733526-l1bnb83vf7qqurpibo0qef2rk3opd3rb.apps.googleusercontent.com";

const Logout = () => {
  const navigate = useNavigate();
  const onSuccess = () => {
    console.log("Logout Success");
    navigate("/");
  };
  return (
    <div id="signOutButton">
      <GoogleLogout
        clientId={clientId}
        buttonText="Logout"
        onLogoutSuccess={onSuccess}
      />
    </div>
  );
};

export default Logout;
