import React from "react";
import { GoogleLogin } from "react-google-login";
import { useNavigate } from "react-router-dom";

const clientId =
  "920329733526-l1bnb83vf7qqurpibo0qef2rk3opd3rb.apps.googleusercontent.com";

const Login = () => {
  const navigate = useNavigate();
  const onSuccess = (res) => {
    console.log("Login Success! current user: ", res.profileObj);
    navigate("/dashboard");
  };
  const onFailure = (res) => {
    console.log("Login Failed! res:", res);
  };

  return (
    <div id="signInButton">
      <GoogleLogin
        clientId={clientId}
        buttonText="Login"
        onSuccess={onSuccess}
        onFailure={onFailure}
        cookiePolicy={"single_host_origin"}
        isSignedIn={true}
      />
    </div>
  );
};

export default Login;
