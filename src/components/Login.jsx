import React, { useState, useEffect } from "react";
import { gapi } from "gapi-script";
import LoginButton from "../Oauth/Login";
import { signOut, signInWithEmailAndPassword } from "firebase/auth";
import { auth } from "../firebaseConfig";

const clientId =
  "920329733526-l1bnb83vf7qqurpibo0qef2rk3opd3rb.apps.googleusercontent.com";

export const Login = (props) => {
  const [loginEmail, setLoginEmail] = useState("");
  const [loginPassword, setLoginPassword] = useState("");
  const [loginError, setLoginError] = useState("");

  async function logout() {
    await signOut(auth);
  }
  async function login() {
    try {
      setLoginError(null);
      await signInWithEmailAndPassword(auth, loginEmail, loginPassword);
    } catch (e) {
      setLoginError(e);
    }
  }

  //   const handleSubmit = (e) => {
  //     e.preventDefault();
  //     console.log(email);
  //   };

  useEffect(() => {
    function start() {
      gapi.client.init({
        clientId: clientId,
        scope: "",
      });
    }
    gapi.load("client:auth2", start);
  }, []);

  return (
    <div className="auth-form-container">
      <h2>Login</h2>
      <form className="login-form">
        <label htmlFor="email">email</label>
        <input
          value={loginEmail}
          onChange={(ev) => setLoginEmail(ev.target.value)}
          type="email"
          placeholder="youremail@gmail.com"
          id="email"
          name="email"
        />
        <label htmlFor="password">password</label>
        <input
          value={loginPassword}
          onChange={(ev) => setLoginPassword(ev.target.value)}
          type="password"
          placeholder="********"
          id="password"
          name="password"
        />
        <button type="submit" onClick={login}>
          Login Now
        </button>
        <div> {loginError?.message} </div>
      </form>
      <button
        className="link-btn"
        onClick={() => props.onFormSwitch("register")}
      >
        Don't have an account? Register here.
      </button>
      <div className="items-center flex flex-col">
        <h1>OR</h1>
        <LoginButton />
      </div>
    </div>
  );
};
