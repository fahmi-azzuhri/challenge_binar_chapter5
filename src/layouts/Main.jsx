import Nav from "../components/Nav";
import { Routes, Route, useLocation } from "react-router-dom";
import Popular from "../pages/Popular";
import Upcoming from "../pages/Upcoming";
import Details from "../pages/Details";
import Searched from "../pages/Searched";
import TopRated from "../pages/TopRated";
import LoginPage from "../components/LoginPage";
import Dashboard from "../components/Dashboard";
// import { AuthContextProvider } from "../context/AuthContext";

function Main() {
  const location = useLocation();
  const isHidden = location.pathname === "/login";
  return (
    <>
      {isHidden ? (
        location.pathname === "/login" && <LoginPage />
      ) : (
        <div>
          <Nav />
          <Routes>
            <Route index element={<Popular />} />
            <Route element={<Upcoming />} path="/upcoming" />
            <Route element={<TopRated />} path="/toprated" />
            <Route element={<Details />} path="/details/:id" />
            <Route element={<Searched />} path="/search/:title" />
            <Route element={<LoginPage />} path="/login" />
            <Route element={<Dashboard />} path="/dashboard" />
          </Routes>
          )
        </div>
      )}
    </>
  );
}

export default Main;
