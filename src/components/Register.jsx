import React, { useState, useEffect } from "react";
import { gapi } from "gapi-script";
import { createUserWithEmailAndPassword } from "firebase/auth";
import { auth } from "../firebaseConfig";

const clientId =
  "920329733526-l1bnb83vf7qqurpibo0qef2rk3opd3rb.apps.googleusercontent.com";

export const Register = (props) => {
  const [registrationEmail, setRegistrationEmail] = useState("");
  const [registrationPassword, setRegistrationPassword] = useState("");
  const [registrationError, setRegistrationError] = useState("");

  async function register() {
    setRegistrationError(null);
    try {
      await createUserWithEmailAndPassword(
        auth,
        registrationEmail,
        registrationPassword
      );
    } catch (e) {
      setRegistrationError(e);
    }
  }

  // const handleSubmit = (e) => {
  //   e.preventDefault();
  //   console.log(email);
  // };

  useEffect(() => {
    function start() {
      gapi.client.init({
        clientId: clientId,
        scope: "",
      });
    }
    gapi.load("client:auth2", start);
  }, []);

  return (
    <div className="auth-form-container">
      <h2>Register</h2>
      <form className="register-form">
        <label htmlFor="email">email</label>
        <input
          value={registrationEmail}
          onChange={(ev) => setRegistrationEmail(ev.target.value)}
          type="email"
          placeholder="youremail@gmail.com"
          id="email"
          name="email"
        />
        <label htmlFor="password">password</label>
        <input
          value={registrationPassword}
          onChange={(ev) => setRegistrationPassword(ev.target.value)}
          type="password"
          placeholder="********"
          id="password"
          name="password"
        />
        <button type="submit" onClick={register}>
          Register Now
        </button>
        <div> {registrationError?.message} </div>
      </form>
      <button className="link-btn" onClick={() => props.onFormSwitch("login")}>
        Already have an account? Login here.
      </button>
    </div>
  );
};
